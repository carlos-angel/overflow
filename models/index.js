"use strict";
const { URI_FIREBASE } = require("../config/config");
const firebase = require("firebase-admin");
const serviceAccount = require("../config/firebase.json");

const { User, Question } = require("./index.models");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: URI_FIREBASE,
});

const db = firebase.database();

module.exports = {
  User: new User(db),
  Question: new Question(db),
};
