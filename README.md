# Overflow

### Descripción:

Pagina web para publicar preguntas con el propósito de recibir y dar feedback

### Instalación:

1. Crear un proyecto en firebase-console y generar el firebase.json el cual se guarda en la carpeta config
2. Generar las variables de entorno definidas en el archivo .env.test
3. Ejecutar el comando `npm install` para instalar las dependencias.
4. Ejecutar el comando `npm start` para iniciar el servidor

### Imágenes del sitio:

![](https://bucket-public-carlos-angel.s3.us-east-2.amazonaws.com/overflow/view-home.png)

![](https://bucket-public-carlos-angel.s3.us-east-2.amazonaws.com/overflow/view-login.png)

![](https://bucket-public-carlos-angel.s3.us-east-2.amazonaws.com/overflow/view-registro.png)

![](https://bucket-public-carlos-angel.s3.us-east-2.amazonaws.com/overflow/view-nueva-pregunta.png)

![](https://bucket-public-carlos-angel.s3.us-east-2.amazonaws.com/overflow/view-pregunta.png)
