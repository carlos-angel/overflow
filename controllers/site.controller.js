"use strict";
const { Question } = require("../models");

async function home(req, h) {
  const data = await req.server.methods.getLast(10);

  return h.view("index", {
    title: "Home",
    user: req.state.user,
    questions: data,
  });
}

function register(req, h) {
  return !req.state.user
    ? h.view("register", { title: "Register" })
    : h.redirect("/");
}

function login(req, h) {
  return !req.state.user
    ? h.view("login", { title: "login", user: req.state.user })
    : h.redirect("/");
}

async function viewQuestion(req, h) {
  let data;
  try {
    data = await Question.getOne(req.params.id);
    if (!data) {
      return notFound(req, h);
    }
  } catch (error) {
    req.error("error", error.message);
  }

  return h.view("question", {
    title: "Detalles de la pregunta",
    user: req.state.user,
    question: data,
    key: req.params.id,
  });
}

function ask(req, h) {
  return !req.state.user
    ? h.redirect("/login")
    : h.view("ask", { title: "Crear pregunta", user: req.state.user });
}

function notFound(req, h) {
  return h.view("404", {}, { layout: "error-layout" }).code(404);
}

function fileNotFound(req, h) {
  const response = req.response;
  if (
    !req.path.startsWith("/api") &&
    response.isBoom &&
    response.output.statusCode === 404
  ) {
    return h.view("404", {}, { layout: "error-layout" }).code(404);
  }

  return h.continue;
}

module.exports = {
  home,
  fileNotFound,
  register,
  login,
  notFound,
  ask,
  viewQuestion,
};
