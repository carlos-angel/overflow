"use strict";
const { writeFile } = require("fs");
const { promisify } = require("util");
const { join } = require("path");
const { create } = require("handlebars");
const { Question } = require("../models");
const uuid = require("uuid");
const write = promisify(writeFile);

async function createQuestion(req, h) {
  if (!req.state.user) {
    return h.redirect("/login");
  }
  let result;
  let filename;

  try {
    if (Buffer.isBuffer(req.payload.image)) {
      filename = `${uuid.v1()}.png`;
      await write(
        join(__dirname, "..", "public", "uploads", filename),
        req.payload.image
      );
    }

    result = await Question.create(req.payload, req.state.user, filename);
    req.log("info", `pregunta creada con el ID: ${result}`);
  } catch (error) {
    req.log("error", error.message);
    return h
      .view("ask", {
        title: "Crear pregunta",
        error: "Problemas creando la pregunta",
      })
      .code(500)
      .takeover();
  }

  return h.redirect(`/question/${result}`);
}

async function answerQuestion(req, h) {
  if (!req.state.user) {
    return h.redirect("/login");
  }
  let result;
  try {
    result = await Question.answer(req.payload, req.state.user);
    req.log("info", `respuesta creada ${result}`);
  } catch (error) {
    req.log("error", error.message);
  }
  return h.redirect(`/question/${req.payload.id}`);
}

async function setAnswerRight(req, h) {
  if (!req.state.user) {
    return h.redirect("/login");
  }

  let result;
  try {
    result = await req.server.methods.setAnswerRight(
      req.params.questionId,
      req.params.answerId,
      req.state.user
    );
    req.log("info", `se actualizo respuesta ${result}`);
  } catch (error) {
    req.log("error", error.message);
  }

  return h.redirect(`/question/${req.params.questionId}`);
}

module.exports = {
  createQuestion,
  answerQuestion,
  setAnswerRight,
};
