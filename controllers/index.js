module.exports = {
  SiteController: require("./site.controller"),
  UserController: require("./user.controller"),
  QuestionController: require("./question.controller"),
};
